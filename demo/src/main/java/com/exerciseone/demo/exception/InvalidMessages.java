package com.exerciseone.demo.exception;

public enum InvalidMessages {
	
	RECORD_NOT_FOUND("No account record exist for given id"),
	INVALID_AMOUNT("Amount is less than zero!");
	
	String message;
	
	InvalidMessages(String message) {
		this.message = message;
	}
	
	public String value() {
		return this.message;
	}

}
