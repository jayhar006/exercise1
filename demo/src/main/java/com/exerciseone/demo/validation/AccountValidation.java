package com.exerciseone.demo.validation;

import com.exerciseone.demo.exception.InvalidAmountException;
import com.exerciseone.demo.exception.InvalidMessages;

public class AccountValidation {

    public void validateAmount(double amount) throws InvalidAmountException {
        if(amount < 0){
            throw new InvalidAmountException(InvalidMessages.INVALID_AMOUNT.value());
        }
    }
}
