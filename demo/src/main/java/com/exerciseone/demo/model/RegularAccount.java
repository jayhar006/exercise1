package com.exerciseone.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.exerciseone.demo.exception.InvalidAmountException;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table
public class RegularAccount extends Account{
	
	@JsonIgnore
	@Column
    private double minimumBalance;
	
	@JsonIgnore
	@Column
    private double penalty;
	
	public RegularAccount() {
		super();
    	this.penalty = 10;
        this.minimumBalance = 500;
        this.setBalance(500);
	}

    @Override
    public void deposit(double amount) throws InvalidAmountException {
        validation.validateAmount(amount);
        this.setBalance(this.getBalance() + amount);
    }

    @Override
    public void withdraw(double amount) throws InvalidAmountException {
        validation.validateAmount(amount);
        this.setBalance(this.getBalance() - amount);
        if(this.getBalance() < this.minimumBalance) {
            this.setBalance(this.getBalance() - this.penalty);
        }
    }
}
