package com.exerciseone.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.exerciseone.demo.exception.InvalidAmountException;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table
public class CheckingAccount extends Account {

	@JsonIgnore
	@Column
    private double minimumBalance;
	
	@JsonIgnore
	@Column
    private double 	penalty;
	
	@JsonIgnore
	@Column
    private double transactionCharge;
    
	@JsonIgnore
    @Column
    private double interestCharge;


    public CheckingAccount() {
    	super();
    	this.penalty = 10;
        this.minimumBalance = 100;
        this.transactionCharge = 1;
        this.interestCharge = 0.03;
        this.setBalance(100);
    }


    @Override
    public void deposit(double amount) throws InvalidAmountException {
        validation.validateAmount(amount);
        this.setBalance(this.getBalance() + amount - this.getTransactionCharge());
    }

    @Override
    public void withdraw(double amount) throws InvalidAmountException {
        validation.validateAmount(amount);
        this.setBalance(this.getBalance() - amount - this.getTransactionCharge());
        if(this.getBalance() < this.minimumBalance) {
            this.setBalance(this.getBalance() - this.penalty);
        }
    }

    @Override
    public void addInterest() {
        this.setBalance(this.getBalance() + (this.getBalance() * interestCharge));
    }
}
