package com.exerciseone.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.exerciseone.demo.exception.InvalidAmountException;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table
public class InterestAccount extends Account{

	@JsonIgnore
	@Column
    private double interestCharge;

    public InterestAccount() {
    	super();
        this.interestCharge = 0.03;
    }

    @Override
    public void deposit(double amount) throws InvalidAmountException {
        validation.validateAmount(amount);
        this.setBalance(this.getBalance() + amount);
    }

    @Override
    public void withdraw(double amount) throws InvalidAmountException {
        validation.validateAmount(amount);
        this.setBalance(this.getBalance() - amount);
    }

    @Override
    public void addInterest() {
        this.setBalance(this.getBalance() + (this.getBalance() * interestCharge));
    }
}
