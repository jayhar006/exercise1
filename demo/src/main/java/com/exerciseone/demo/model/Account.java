package com.exerciseone.demo.model;

import com.exerciseone.demo.exception.InvalidAmountException;
import com.exerciseone.demo.validation.AccountValidation;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Random;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		include = JsonTypeInfo.As.PROPERTY,
		property = "type")
@JsonSubTypes({
	@Type(value = RegularAccount.class, name = "regular"),
	@Type(value = CheckingAccount.class, name = "checking"),
	@Type(value = InterestAccount.class, name = "interest")
})	
@Getter
@Setter
@Table
public abstract class Account extends AuditModel{
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column
	private long id;
	
    @Column
	private String name;
	
    @Column
	private String acctNumber;
	
    @Column
	private double balance;
	
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Transient
    protected AccountValidation validation;

	public abstract void deposit(double amount) throws InvalidAmountException;
	public abstract void withdraw(double amount) throws InvalidAmountException;
	
	public Account() {
		Random rand = new Random();
		long num = (long)(rand.nextDouble() * 10000000000000000L);
		this.acctNumber = String.valueOf(num);
        validation = new AccountValidation();
	}

	public void addInterest() {
		this.setBalance(balance);
	}

}
