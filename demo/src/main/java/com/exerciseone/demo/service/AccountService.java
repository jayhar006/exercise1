package com.exerciseone.demo.service;

import com.exerciseone.demo.exception.InvalidAmountException;
import com.exerciseone.demo.exception.InvalidMessages;
import com.exerciseone.demo.exception.RecordNotFoundException;
import com.exerciseone.demo.model.Account;
import com.exerciseone.demo.model.AccountTransaction;
import com.exerciseone.demo.repository.AccountRepository;
import com.exerciseone.demo.repository.AccountTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountTransactionRepository accountTransactionRepository;


    public List<Account> getAllAccounts() {
        List<Account> accountList = accountRepository.findAll();
        if (accountList.size() > 0) {
            return accountList;
        } else {
            return new ArrayList<Account>();
        }
    }

    public Account getAccountById(Long id) throws RecordNotFoundException {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            return account.get();
        } else {
            throw new RecordNotFoundException(InvalidMessages.RECORD_NOT_FOUND.value());
        }
    }

    public Account createOrUpdateAccount(Account entity) throws RecordNotFoundException {
        Optional<Account> account = accountRepository.findById(entity.getId());
        if (account.isPresent()) {
            Account newEntity = account.get();
            newEntity.setName(entity.getName());
            newEntity.setBalance(entity.getBalance());
            newEntity = accountRepository.save(newEntity);
            return newEntity;
        } else {
            entity = accountRepository.save(entity);
            return entity;
        }
    }

    public void deleteByAccountId(Long id) throws RecordNotFoundException {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            accountRepository.deleteById(id);
        } else {
            throw new RecordNotFoundException(InvalidMessages.RECORD_NOT_FOUND.value());
        }
    }
    
    public Account depositOrWithdrawAccount(Long id, AccountTransaction transaction) throws RecordNotFoundException, InvalidAmountException {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            Account newEntity = account.get();
            double amount = transaction.getAmount();
            if (transaction.getType().equalsIgnoreCase("deposit")) {
            	newEntity.deposit(amount);
            } else {
            	newEntity.withdraw(amount);
            }
            transaction.setAccount(newEntity);
            accountTransactionRepository.save(transaction);
            newEntity = accountRepository.save(newEntity);
            return newEntity;
        } else {
            throw new RecordNotFoundException(InvalidMessages.RECORD_NOT_FOUND.value());
        }
    }

    public Account createOrUpdateTransaction(Account entity) throws RecordNotFoundException {
        Optional<Account> account = accountRepository.findById(entity.getId());
        if (account.isPresent()) {
            Account newEntity = account.get();
            newEntity.setName(entity.getName());
            newEntity.setBalance(entity.getBalance());
            newEntity = accountRepository.save(newEntity);
            return newEntity;
        } else {
            entity = accountRepository.save(entity);
            return entity;
        }
    }

    public void deleteByAccountTransactionById(Long id) throws RecordNotFoundException {
        Optional<AccountTransaction> accountTransaction = accountTransactionRepository.findById(id);
        if(accountTransaction.isPresent()){
            accountTransactionRepository.deleteById(id);
        }
        else{
            throw new RecordNotFoundException(InvalidMessages.RECORD_NOT_FOUND.value());
        }
    }
}
