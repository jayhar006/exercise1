package com.exerciseone.demo.repository;

import com.exerciseone.demo.model.AccountTransaction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountTransactionRepository extends JpaRepository<AccountTransaction, Long> {
    Page<AccountTransaction> findAll(Pageable pageable);
    Page<AccountTransaction> findAllByAccountId(Long accountId, Pageable pageable);
}
