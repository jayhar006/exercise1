package com.exerciseone.demo.controller;

import com.exerciseone.demo.exception.InvalidAmountException;
import com.exerciseone.demo.exception.RecordNotFoundException;
import com.exerciseone.demo.model.Account;
import com.exerciseone.demo.model.AccountTransaction;
import com.exerciseone.demo.model.RegularAccount;
import com.exerciseone.demo.repository.AccountRepository;
import com.exerciseone.demo.repository.AccountTransactionRepository;
import com.exerciseone.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountTransactionRepository accountTransactionRepository;

    
//    @GetMapping
//    public ResponseEntity<List<Account>> getAllAccounts() {
//        List<Account> list = accountService.getAllAccounts();
//        return new ResponseEntity<List<Account>>(list, new HttpHeaders(), HttpStatus.OK);
//    }

    @GetMapping
    public Page<Account> getAllAccounts(Pageable pageable) {
        return accountRepository.findAll(pageable);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Account> getAccountById(@PathVariable("id") Long id) throws RecordNotFoundException {
        Account entity = accountService.getAccountById(id);
        return new ResponseEntity<Account>(entity, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Account> createOrUpdateAccount(@RequestBody Account account) throws RecordNotFoundException {
        Account updated = accountService.createOrUpdateAccount(account);
        return new ResponseEntity<Account>(updated, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Account> deleteAccountById(@PathVariable("id") Long id) throws RecordNotFoundException {
        accountService.deleteByAccountId(id);
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }
    
    @PostMapping("/{id}/transactions")
    public ResponseEntity<Account> transactions(@PathVariable("id") Long id, @RequestBody AccountTransaction transaction) throws RecordNotFoundException, InvalidAmountException {
        Account updated = accountService.depositOrWithdrawAccount(id, transaction);
        return new ResponseEntity<Account>(updated, new HttpHeaders(), HttpStatus.OK);
    }



    @GetMapping("/{accountId}/transactions")
    public Page<AccountTransaction> getTransactions(@PathVariable (value = "accountId") Long id, Pageable pageable) throws RecordNotFoundException, InvalidAmountException {
        return accountTransactionRepository.findAllByAccountId(id, pageable);
    }

    @DeleteMapping("/{accountId}/transactions")
    public ResponseEntity<AccountTransaction> deleteAccountTransactionById(@PathVariable("accountId") Long id) throws RecordNotFoundException {
        accountService.deleteByAccountTransactionById(id);
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }

    /*
    * To test the job comment the cron and uncomment the fixedDelay
    * also remove the if condition that checks end date of the month
    * */
    @Scheduled(cron = "0 00 00 28-31 * ?")
//    @Scheduled(fixedDelay = 10000)
    public void addInterestJob() throws RecordNotFoundException {
        System.out.println("Job Run");
        final Calendar c = Calendar.getInstance();
        if (c.get(Calendar.DATE) == c.getActualMaximum(Calendar.DATE)) {
            List<Account> list = accountService.getAllAccounts();
            list.stream()
                    .filter(acn -> acn.getBalance() > 0 && !(acn instanceof RegularAccount))
                    .forEach(account -> {
                        System.out.println(account.getName());
                        System.out.println(account.getBalance());
                        Account addInt = account;
                        addInt.addInterest();
                        try {
                            accountService.createOrUpdateAccount(addInt);
                        } catch (RecordNotFoundException e) {
                            e.printStackTrace();
                        }
                        System.out.println(addInt.getBalance());
                    });
        }
    }

}
